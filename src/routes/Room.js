import React, { useEffect, useRef, useState } from "react";
import io from "socket.io-client";
import "./room.css";

var ICE_config = {
  iceServers: [
    {
      urls: "stun:stun.l.google.com:19302",
    },
    {
      urls: "stun:stun1.l.google.com:19302",
    },
  ],
};

const videoConstraints = {
  height: window.innerHeight / 2,
  width: window.innerWidth / 2,
};

const Room = (props) => {
  const socketRef = useRef();
  const videoCtr = useRef();
  const remoteVideoCtr = useRef();
  const audioTrack = useRef();
  const videoTrack = useRef();
  const connection = useRef();
  const message = useRef();
  const remoteStream = useRef();
  const rtpSender = useRef();
  const [isStop, setCameraLabelStatus] = useState(true);
  const roomID = props.match.params.roomID;

  useEffect(() => {
    socketRef.current = io.connect("http://10.0.0.37:8000");
    // socketRef.current.emit("create_room",socket.id)
  }, []);

  useEffect(() => {
    const hitSocket = async () => {
      socketRef.current.on("join-user", async (data) => {
        message.current = JSON.parse(data);
        if (message.current.rejected) {
        } else if (message.current.answer) {
          await connection.current.setRemoteDescription(
            new RTCSessionDescription(message.current.answer)
          );
        } else if (message.current.offer) {
          let confirmation = true;

          if (!audioTrack.current) {
            confirmation = window.confirm("want to continue");
            if (confirmation) {
              await startWithAudio();
              if (audioTrack.current) {
                if (!connection.current) {
                  await createConnection(false);
                }
                connection.current.addTrack(audioTrack.current);
              }
            } else {
              socketRef.current.emit(
                "join-user",
                JSON.stringify({ rejected: "true" })
              );
            }
          }
          if (audioTrack.current) {
            if (!connection.current) {
              await createConnection(false);
            }
            await connection.current.setRemoteDescription(
              new RTCSessionDescription(message.current.offer)
            );

            let answer = await connection.current.createAnswer();
            await connection.current.setLocalDescription(answer);
            socketRef.current.emit(
              "join-user",
              JSON.stringify({ answer: answer })
            );
          }
        } else if (message.current.iceCandidate) {
          if (!connection.current) {
            await createConnection(false);
          }
          try {
            await connection.current.addIceCandidate(
              message.current.iceCandidate
            );
          } catch (error) {
            console.log("erro", error);
          }
        }
      });
    };
    hitSocket();
  }, []);

  const startConnection = async () => {
    await startWithAudio();
    await createConnection(true);
  };
  const startWithAudio = async () => {
    try {
      var aStream = await navigator.mediaDevices.getUserMedia({
        video: false,
        audio: true,
      });
      audioTrack.current = aStream.getAudioTracks()[0];
      audioTrack.current.onmute = function (e) {
        console.log(e);
      };
      audioTrack.current.onunmuted = function (e) {
        console.log(e);
      };
    } catch (error) {
      console.log("Audio Error", error);
    }
  };

  const createConnection = async (flag) => {
    connection.current = new RTCPeerConnection(ICE_config);
    if (flag) await createUserOffer();

    connection.current.onicecandidate = function (event) {
      if (event.candidate) {
        socketRef.current.emit(
          "join-user",
          JSON.stringify({ iceCandidate: event.candidate })
        );
      }
    };

    connection.current.onnegotiationneeded = async function (event) {
      debugger;
      await createUserOffer();
    };

    connection.current.ontrack = function (event) {
      if (!remoteStream.current) remoteStream.current = new MediaStream();
      if (event.track.kind == "video") {
        remoteStream.current
          .getVideoTracks()
          .forEach((t) => remoteStream.current.removeTrack(t));
      }
      remoteStream.current.addTrack(event.track);
      remoteStream.current.getTracks().forEach((t) => console.log(t));
      remoteVideoCtr.current.srcObject = null;
      remoteVideoCtr.current.srcObject = remoteStream.current;
      remoteVideoCtr.current.load();
    };
    connection.current.onicecandidateerror = function (event) {
      console.log("onicecandidateerror", event);
    };

    connection.current.onicegatheringstatechange = function (event) {
      console.log("onicegatheringstatechange", event);
    };

    connection.current.onconnectionstatechange = function (event) {
      console.log("onconnectionstatechange", event);
    };
  };

  const createUserOffer = async () => {
    var offer = await connection.current.createOffer();
    await connection.current.setLocalDescription(offer);
    socketRef.current.emit(
      "join-user",
      JSON.stringify({ offer: connection.current.localDescription })
    );
  };

  const startStopCamera = async () => {
    setCameraLabelStatus(!isStop);
    if (videoTrack.current) {
      videoTrack.current.stop();
      videoTrack.current = null;
      videoCtr.current.srcObject = null;
      if (rtpSender.current && connection.current) {
        connection.current.removeTrack(rtpSender.current);
        rtpSender.current = null;
      }
      return;
    }
    navigator.mediaDevices
      .getUserMedia({ video: videoConstraints, audio: false })
      .then((stream) => {
        videoTrack.current = stream.getVideoTracks()[0];
        setLocalVideo(true);
        // videoCtr.current.srcObject = stream;
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const setLocalVideo = (isVideo) => {
    var currentTrack;
    if (isVideo) {
      if (videoTrack.current) {
        videoCtr.current.srcObject = new MediaStream([videoTrack.current]);
        currentTrack = videoTrack.current;
      }
    }

    if (
      rtpSender.current &&
      rtpSender.current.track &&
      currentTrack &&
      connection.current
    ) {
      rtpSender.current.replaceTrack(currentTrack);
    } else {
      if (currentTrack && connection.current)
        rtpSender.current = connection.current.addTrack(currentTrack);
    }
  };

  return (
    <div className="container">
      <video
        autoPlay
        muted
        controls
        ref={videoCtr}
        width="480"
        height="200"
      ></video>
      <video
        autoPlay
        muted
        controls
        ref={remoteVideoCtr}
        width="480"
        height="200"
        style={{ marginLeft: "20px" }}
      ></video>

      <button onClick={startStopCamera}>
        {" "}
        {isStop ? "Start Camera" : "Stop Camera"}
      </button>
      <button onClick={startConnection}>Start connection</button>
    </div>
  );
};

export default Room;
